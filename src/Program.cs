﻿using System;
using System.Threading;

namespace program
{
    public class Program
    {
        static void Main(string[] args)
        {
            string answer = "";
            Balance balance = new Balance(1000);

            Console.WriteLine("[i] Your account balance is: {0:D}", balance.value);
            do
            {
                Console.WriteLine("[?] Enter an amount to withdraw (e.g. 10, 1000):");
                Amount amount = new Amount(Convert.ToInt32(Console.ReadLine()));

                if (!performTransfer(amount, balance))
                {
                    Console.WriteLine("[e] The amount is higher than your account balance");
                    return;
                }
                Console.WriteLine("[?] Do you want to make another withdraw (y or n)?");
                answer = Console.ReadLine();

            } while(answer.Equals("y"));
        }

        /**
         * Checks if a given amount can be withdrawed
         * if so, it will call a service to make the withdraw
         * @return boolean
         */
        public static bool performTransfer(Amount amount, Balance balance)
        {
            if (amount.value <= balance.value)
            {
                Console.WriteLine("[i] Going to make the withdraw");
                Thread t = new Thread(() => doTransferService(amount, balance));
                t.Start();
                return true;
            }
            return false;
        }
        private static void doTransferService(Amount amount, Balance balance)
        {
            //The below line is a service delay simulator. Do not change.
            System.Threading.Thread.Sleep(1000);
            balance.value = balance.value - amount.value;
            Console.WriteLine("[i] Successfully transfered: {0:C}", amount.value);
            Console.WriteLine("[i] Your new balance is: {0:D}", balance.value);
        }
    }
}
