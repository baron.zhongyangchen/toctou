using System;
using NUnit.Framework;

namespace program.Tests
{
    [TestFixture, Category("usability")]
    public class programUsabilityTest
    {
        [Test]
        public void withdraw_1001_not_allowed()
        {
            Balance balance = new Balance(1000);
            Amount amount = new Amount(1001);
            Assert.IsFalse(Program.performTransfer(amount, balance));
        }
        [Test]
        public void withdraw_two_500_zeros_balance()
        {
            Balance balance = new Balance(1000);
            Amount amount = new Amount(500);
            Assert.That(Program.performTransfer(amount, balance), Is.True.After(1000));
            Assert.That(Program.performTransfer(amount, balance), Is.True.After(1500));
            Assert.IsTrue(balance.value == 0);
        }
    }
}
